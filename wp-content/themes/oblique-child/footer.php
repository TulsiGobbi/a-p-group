<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Oblique
 */
?>

		</div>
	</div><!-- #content -->

	<?php do_action( 'oblique_footer_svg' ); ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
		<p style="color:#ffffff;"> A&P Group Srls - Italy - Via XX Settembre 79 - 47921 Rimini (RN) - P.IVA 04279950408 </p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
